#include "mcmc.hpp"
using namespace std;


////////////////////////////////////////////////////////////////////////////////
// Proposals
////////////////////////////////////////////////////////////////////////////////
Proposal::Proposal(string type): type(type) { }


LogNormalProposal::LogNormalProposal(): Proposal("lognormal") {}


void LogNormalProposal::propose(double value, double random_walk_step) {
  proposed_value = value * exp(random_walk_step);
  log_proposal = random_walk_step; // log(proposed_value) - log(value)
}


NormalProposal::NormalProposal(): Proposal("normal") {}


void NormalProposal::propose(double value, double random_walk_step) {
  proposed_value = value + random_walk_step;
  log_proposal = 0.0;
}


////////////////////////////////////////////////////////////////////////////////
// Priors
////////////////////////////////////////////////////////////////////////////////
Prior::Prior(string type): type(type) {}


UniformPrior::UniformPrior(double lower_bound, double upper_bound):
  Prior("uniform"), lower_bound(lower_bound), upper_bound(upper_bound) {}


void UniformPrior::compute_log_ratio(double old_value, double new_value) {
  log_ratio = 0.0;
  outside_support = false;
  if (new_value < lower_bound || new_value > upper_bound) {
    log_ratio = -1.0;
    outside_support = true;
  }
}


double UniformPrior::draw(mt19937_64& gen) {
  uniform_real_distribution<double> dist(lower_bound, upper_bound);
  return dist(gen);
}


NormalPrior::NormalPrior(double mean, double std): Prior("normal"), mean(mean),
  std(std) {}


void NormalPrior::compute_log_ratio(double old_value, double new_value) {
  log_ratio = old_value - new_value;
  log_ratio *= ((old_value + new_value) * 0.5 - mean);
  log_ratio /= (std * std);
  outside_support = false;
}


double NormalPrior::draw(mt19937_64& gen) {
  normal_distribution<double> dist(mean, std);
  return dist(gen);
}


GammaPrior::GammaPrior(double shape, double scale): Prior("gamma"),
  shape(shape), scale(scale) {}


void GammaPrior::compute_log_ratio(double old_value, double new_value) {
  if (new_value <= 0.0) {
    log_ratio = -1.0;
    outside_support = true;
    return;
  }
  log_ratio = (shape - 1.0) * log(new_value / old_value)
    - (new_value - old_value) / scale;
  outside_support = false;
}


double GammaPrior::draw(mt19937_64& gen) {
  gamma_distribution<double> dist(shape, scale);
  return dist(gen);
}


////////////////////////////////////////////////////////////////////////////////
// MCMC (public interface)
////////////////////////////////////////////////////////////////////////////////
MCMC::MCMC(MCMCAbstractModel& model): model(&model) {
  n_params = this->model->get_n_params();
  for (size_t i = 0; i < n_params; ++i) {
    param_names.push_back("p" + to_string(i));
    step_scales.push_back(1.0);
    n_moves_accepted.push_back(0.0);
    n_moves_proposed.push_back(0.0);
    acceptance_rates.push_back(0.0);
    proposals.push_back(new LogNormalProposal());
    priors.push_back(new UniformPrior(0.0, 1.0));
  }
  norm_dist = normal_distribution<double>(0.0, 1.0);
  unif_dist = uniform_real_distribution<double>(0.0, 1.0);
}


void MCMC::set_proposal(size_t i, string type) {
  if (is_out_of_bounds(i)) {
    cout << "WARNING: out of bound index for 'set_proposal'!" << endl;
    return;
  }
  if (type == "lognormal" && proposals[i]->type != "lognormal") {
    delete proposals[i];
    proposals[i] = new LogNormalProposal();
  } else if (type == "normal" && proposals[i]->type != "normal") {
    delete proposals[i];
    proposals[i] = new NormalProposal();
  } else {
    cout << "WARNING: unknown proposal!" << endl;
  }
}


void MCMC::set_prior(size_t i, string type, double param1, double param2) {
  if (is_out_of_bounds(i)) {
    cout << "WARNING: out of bound index for 'set_prior'!" << endl;
    return;
  }
  if (type == "uniform") {
    delete priors[i];
    priors[i] = new UniformPrior(param1, param2);
  } else if (type == "normal") {
    if (param2 > 0.0) {
      delete priors[i];
      priors[i] = new NormalPrior(param1, param2);
    } else {
      cout << "WARNING: negative std for 'set_prior'!" << endl;
    }
  } else if (type == "gamma") {
    if (param1 > 0.0 && param2 > 0.0) {
      delete priors[i];
      priors[i] = new GammaPrior(param1, param2);
    } else {
      cout << "WARNING: negative shape/scale for 'set_prior'!" << endl;
    }
  } else {
    cout << "WARNING: unknown prior!" << endl;
  }
}


void MCMC::set_parameter_name(size_t i, string name) {
  if (is_out_of_bounds(i)) {
    cout << "WARNING: out of bound index for 'set_parameter_name'!" << endl;
    return;
  }
  param_names[i] = name;
}


void MCMC::set_step_scale(size_t i, double step_scale) {
  if (is_out_of_bounds(i)) {
    cout << "WARNING: out of bound index for 'set_step_scale'!" << endl;
    return;
  }
  if (step_scale > 0.0) {
    step_scales[i] = step_scale;
  } else {
    cout << "WARNING: negative value for 'set_step_scale'!" << endl;
  }
}


void MCMC::run(size_t n_iter, size_t thinning_factor, string output_fname,
    size_t seed, bool draw_initial_params, bool verbose) {
  initialize_run(output_fname, seed, draw_initial_params, verbose);

  for (size_t iter = 0; iter < n_iter; ++iter) {
    if (verbose) cout_progress(iter, n_iter);
    update_parameters();
    if (with_output && (iter % thinning_factor == 0)) {
      int output_iter = (iter + 1) / thinning_factor;
      write_to_file(output_iter);
    }
  }

  finalize_run(verbose);
}


void MCMC::run_adaptive(size_t n_iter, size_t n_iter_adaptive,
    size_t thinning_factor, string output_fname, double opt_acceptance_rate,
    size_t seed, bool draw_initial_params, bool verbose) {
  assert(n_iter > n_iter_adaptive);
  assert(n_iter_adaptive > 1);
  initialize_run(output_fname, seed, draw_initial_params, verbose);

  for (size_t iter = 0; iter < n_iter; ++iter) {
    if (verbose) cout_progress(iter, n_iter);
    update_parameters();
    if (iter < n_iter_adaptive) {
      adapt_steps(iter, n_iter_adaptive, opt_acceptance_rate);
    }
    if (with_output && (iter % thinning_factor == 0)) {
      int output_iter = (iter + 1) / thinning_factor;
      write_to_file(output_iter);
    }
  }

  finalize_run(verbose);
}


void MCMC::optimize_step_scales(size_t n_iter, size_t max_runs,
    size_t thinning_factor, string output_fname, double opt_acceptance_rate,
    double tolerance, size_t seed, bool draw_initial_params, bool verbose) {
  size_t repeat = 0;
  size_t decay_start = max_runs / 4;
  double delta = 1.0;
  while (true) {
    ++repeat;
    if (repeat >= decay_start) delta = 1.0 / (repeat - decay_start + 1);
    cout << "MCMC run " << repeat << endl;
    run(n_iter, thinning_factor, output_fname, seed, draw_initial_params,
      verbose);
    bool is_OK = true;
    for (size_t i = 0; i < n_params; ++i) {
      double curr_AR = acceptance_rates[i];
      double diff = curr_AR - opt_acceptance_rate;
      // Increase step scale if acceptance rate is too high, decrease viceversa
      step_scales[i] *= 1.0 + delta * diff;
      is_OK = is_OK && (abs(diff) <= tolerance);
    }
    if (is_OK || repeat >= max_runs) break;
  }
}


MCMC::~MCMC() {
  for (size_t i = 0; i < n_params; ++i) {
    delete proposals[i];
    delete priors[i];
  }
}


////////////////////////////////////////////////////////////////////////////////
// MCMC (internals)
////////////////////////////////////////////////////////////////////////////////
void MCMC::initialize_run(string output_fname, size_t seed,
    bool draw_initial_params, bool verbose) {
  // Initialize output (if necessary)
  with_output = false;
  if (output_fname != "") {
    with_output = true;
    output_file.open(output_fname);
    // Add header
    output_file << "iteration,loglik";
    if (n_params > 0) {
      output_file << ",";
      for (size_t i = 0; i < n_params; ++i) {
        output_file << param_names[i] << ",";
      }
      for (size_t i = 0; i < (n_params - 1); ++i) {
        output_file << ("acceptance_" + param_names[i]) << ",";
      }
      output_file << ("acceptance_" + param_names[n_params - 1]) << endl;
    } else {
      output_file << endl;
    }
  }

  // Reset seed and acceptance variables
  gen.seed(seed);
  for (size_t i = 0; i < n_params; ++i) {
    n_moves_accepted[i] = 0.0;
    n_moves_proposed[i] = 0.0;
    acceptance_rates[i] = 0.0;
  }

  if (verbose) {
    size_t width = 51;
    cout_pretty_line(width, '-');
    cout << "Starting MCMC" << endl;
    cout_pretty_line(width, '-');
  }

  if (draw_initial_params) {
    for (size_t i = 0; i < n_params; ++i) {
      model->set_param(i, priors[i]->draw(gen));
    }
  }

  log_lik = model->compute_log_likelihood();
  const ostringstream default_format; // Default format
  cout << "LL at the beginning = " << scientific << setprecision(4) << log_lik
    << endl;
  cout.copyfmt(default_format); // Back to default format
}


void MCMC::update_parameters() {
  for (size_t i = 0; i < n_params; ++i) {
    if (step_scales[i] > 0.0) {
      n_moves_accepted[i] += update_parameter(i);
    }
    n_moves_proposed[i] += 1.0;
    acceptance_rates[i] =
      n_moves_accepted[i] / n_moves_proposed[i];
  }
}


double MCMC::update_parameter(size_t i) {
  double old_value = model->get_param(i);
  double random_walk_step = step_scales[i] * norm_dist(gen);

  proposals[i]->propose(old_value, random_walk_step);
  double new_value = proposals[i]->proposed_value;
  double log_proposal = proposals[i]->log_proposal;

  priors[i]->compute_log_ratio(old_value, new_value);
  bool outside_support = priors[i]->outside_support;
  double log_ratio = priors[i]->log_ratio;

  // Immediately reject if outside prior support
  if (outside_support) return 0.0;

  model->set_param(i, new_value);
  double new_log_lik = model->compute_log_likelihood();
  double log_proba = new_log_lik - log_lik + log_ratio + log_proposal;

  if (log(unif_dist(gen)) < log_proba) {
    // Accept move
    log_lik = new_log_lik;
    return 1.0;
  }
  // Reject move
  model->set_param(i, old_value);
  return 0.0;
}


void MCMC::adapt_steps(size_t iter, size_t n_iter_adaptive,
    double opt_acceptance_rate) {
  double delta = 1.0 - iter / double(n_iter_adaptive);
  for (size_t i = 0; i < n_params; ++i) {
    double curr_AR = acceptance_rates[i];
    double diff = curr_AR - opt_acceptance_rate;
    // Increase step scale if acceptance rate is too high, decrease viceversa
    step_scales[i] *= 1.0 + delta * diff;
  }
}


void MCMC::write_to_file(int iter) {
  output_file << iter << "," << log_lik;
  if (n_params > 0) {
    output_file << ",";
    for (size_t i = 0; i < n_params; ++i) {
      output_file << model->get_param(i) << ",";
    }
    for (size_t i = 0; i < (n_params - 1); ++i) {
      output_file << acceptance_rates[i] << ",";
    }
    output_file << acceptance_rates[n_params - 1] << endl;
  } else {
    output_file << endl;
  }
}


void MCMC::finalize_run(bool verbose) {
  if (with_output) output_file.close();
  const ostringstream default_format; // Default format

  // Print timing and parameters
  cout << "LL at the end = " << scientific << setprecision(4) << log_lik
    << endl;

  if (verbose) {
    cout << endl;
    cout_pretty_table();
  }
  cout.copyfmt(default_format); // Back to default format
}


bool MCMC::is_out_of_bounds(size_t i) const {
  return (i >= n_params) ? true : false;
}


void MCMC::cout_pretty_line(size_t width, char ch) const {
  for (size_t curr = 0; curr < width; ++curr) cout << "-";
  cout << endl;
}


void MCMC::cout_progress(size_t iter, size_t n_iter) const {
  double progress = 100.0 * (iter + 1.0) / n_iter;
  if (abs(progress - floor(progress)) < 1e-6) {
    const ostringstream default_format; // Default format
    // Write percentage of work done
    cout << "Completed " << fixed << setfill(' ') << setw(3) <<
      setprecision(0) << progress << "%" << endl;
    cout.copyfmt(default_format); // Back to default format
  }
}


void MCMC::cout_pretty_table() const {
  size_t width = 51;

  // Header
  cout_pretty_line(width, '-');
  cout << "| Param |        Value |      RWStep |   Acc Rate |" << endl;
  cout_pretty_line(width, '-');

  // Table body
  for (size_t i = 0; i < n_params; ++i) {
    cout << "|" << setfill(' ') << setw(6) << i << " | ";
    cout << scientific << setprecision(4) << setw(12) <<
      model->get_param(i) << " |  " << step_scales[i] << " |  " <<
      acceptance_rates[i] << "|" << endl;
  }
  cout_pretty_line(width, '-');
}
