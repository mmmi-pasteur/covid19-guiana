#ifndef mcmc_hpp_
#define mcmc_hpp_

#include <iostream>
#include <cassert>
#include <vector>
#include <set>
#include <string>
#include <random>
#include <cmath>
#include <iomanip>
#include <fstream>
#include <sstream>

////////////////////////////////////////////////////////////////////////////////
// MCMC abstract model
////////////////////////////////////////////////////////////////////////////////
struct MCMCAbstractModel {
  virtual size_t get_n_params() = 0;
  virtual double get_param(size_t i) = 0;
  virtual void set_param(size_t i, double value) = 0;
  virtual double compute_log_likelihood() = 0;
};


////////////////////////////////////////////////////////////////////////////////
// Proposals
////////////////////////////////////////////////////////////////////////////////
struct Proposal {
  std::string type;
  double proposed_value;
  double log_proposal;
  Proposal(std::string type);
  virtual void propose(double value, double random_walk_step) = 0;
  virtual ~Proposal() {}
};


struct LogNormalProposal: public Proposal {
  LogNormalProposal();
  void propose(double value, double random_walk_step);
};


struct NormalProposal: public Proposal {
  NormalProposal();
  void propose(double value, double random_walk_step);
};


////////////////////////////////////////////////////////////////////////////////
// Priors
////////////////////////////////////////////////////////////////////////////////
struct Prior {
  std::string type;
  bool outside_support;
  double log_ratio;
  Prior(std::string prior_type);
  virtual void compute_log_ratio(double old_value, double new_value) = 0;
  virtual double draw(std::mt19937_64& gen) = 0;
  virtual ~Prior() {}
};


struct UniformPrior: public Prior {
  double lower_bound;
  double upper_bound;
  UniformPrior(double lower_bound, double upper_bound);
  void compute_log_ratio(double old_value, double new_value);
  double draw(std::mt19937_64& gen);
};


struct NormalPrior: public Prior {
  double mean;
  double std;
  NormalPrior(double mean, double std);
  void compute_log_ratio(double old_value, double new_value);
  double draw(std::mt19937_64& gen);
};


struct GammaPrior: public Prior {
  double shape;
  double scale;
  GammaPrior(double shape, double scale);
  void compute_log_ratio(double old_value, double new_value);
  double draw(std::mt19937_64& gen);
};


////////////////////////////////////////////////////////////////////////////////
// MCMC
////////////////////////////////////////////////////////////////////////////////
struct MCMC {
  // -------------------- Public interface --------------------
  // Constructor
  MCMC(MCMCAbstractModel& model);

  // Proposal choices are: "lognormal" (default) or "normal"
  void set_proposal(size_t i, std::string type);

  // Prior choices are: "uniform", "normal" or "gamma"
  // Default choice is Uniform(0, 1)
  // If prior = "uniform" => param1 = lower_bound, param2 = upper_bound
  // If prior = "normal" => param1 = mean, param2 = standard deviation
  // If prior = "gamma" => param1 = shape, param2 = scale
  // Parametrization of gamma function corresponds to:
  //   PDF(x) ~ x^(shape - 1) exp(-x / scale)
  void set_prior(size_t i, std::string type, double param1, double param2);

  // To assign names to parameters
  void set_parameter_name(size_t i, std::string name);

  // Random walk step scale
  void set_step_scale(size_t i, double step_scale);

  // Run the MCMC
  void run(size_t n_iter, size_t thinning_factor = 1,
    std::string output_fname = "", size_t seed = 0,
    bool draw_initial_params = false, bool verbose = true);

  // Run the MCMC while also adapting the random walk steps
  void run_adaptive(size_t n_iter, size_t n_iter_adaptive,
    size_t thinning_factor = 1, std::string output_fname = "",
    double opt_acceptance_rate = 0.24, size_t seed = 0,
    bool draw_initial_params = false, bool verbose = true);

  // Run the MCMC multiple times, adjusting the random walk step after each run
  //   so that the acceptance rates are close to opt_acceptance_rate
  void optimize_step_scales(size_t n_iter = 100, size_t max_runs = 100,
    size_t thinning_factor = 1, std::string output_fname = "",
    double opt_acceptance_rate = 0.24, double tolerance = 0.05,
    size_t seed = 0, bool draw_initial_params = false, bool verbose = true);

  // Destructor
  ~MCMC();

  // -------------------- Internals --------------------
  MCMCAbstractModel *model;
  double log_lik;
  size_t n_params;
  std::vector<std::string> param_names;
  std::vector<double> step_scales;
  std::vector<double> n_moves_accepted;
  std::vector<double> n_moves_proposed;
  std::vector<double> acceptance_rates;
  std::vector<Proposal*> proposals;
  std::vector<Prior*> priors;
  std::mt19937_64 gen;
  std::normal_distribution<double> norm_dist;
  std::uniform_real_distribution<double> unif_dist;
  bool with_output;
  std::ofstream output_file;

  // Helper methods for running the MCMC
  void initialize_run(std::string output_fname, size_t seed,
    bool draw_initial_params, bool verbose);
  void update_parameters();
  double update_parameter(size_t i);
  void adapt_steps(size_t iter, size_t n_iter_adaptive,
    double opt_acceptance_rate);
  void write_to_file(int iter);
  void finalize_run(bool verbose);

  // Varia
  bool is_out_of_bounds(size_t i) const;
  void cout_pretty_line(size_t width, char ch) const;
  void cout_progress(size_t iter, size_t n_iter) const;
  void cout_pretty_table() const;
};

#endif // mcmc_hpp_