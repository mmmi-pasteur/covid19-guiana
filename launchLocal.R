#!/usr/bin/env Rscript

# ------------------------------------------------------------------------------
# GENERAL (FIXED) PARAMETERS
# ------------------------------------------------------------------------------
# File containing the data
# The file should have four columns (no header, one row per day):
#  new hospital admissions, new ICU admissions, number of patients (general
#  ward), number of patients in ICU
fdata <- "Data/guyane20200825.txt"

# Population of French Guyana
population <- 290691

# Time step for solving the ODE system
dt <- 0.1

# Number of time steps for the simulations (start of simulations is April 21st,
#  193 corresponds to October 31st 2020)
n_steps <- 193

# Data points to discard (from the end of input file)
remove_last_n <- 1

# Number of MCMC steps
mcmc_steps <- 100000

# Number of MCMC adaptive steps (must be < number of MCMC steps)
mcmc_adaptive_steps <- 50000

# MCMC thinning factor (must be >= 1)
mcmc_thinning <- 20

# File to write MCMC output
fmcmc <- "Output/mcmc.csv"

# File to write simulations output
fsims <- "Output/mcmc_sims.csv"

# ------------------------------------------------------------------------------
# PARAMETERS FOR INFERENCE
# ------------------------------------------------------------------------------
# If the value is set to -1 the parameter is added to the list of parameters
# to be estimated via the MCMC, otherwise it is fixed at the chosen value.
# ------------------------------------------------------------------------------
# Rate S -> E1
g1 <- 1. / 4.

# Rate E2 -> I
g2 <- 1. / 1.

# Rate I -> R
g3 <- 1. / 3.

# Rate hospital admission
gToHosp <- 1. / 4.

# 2 x rate hospital -> R
gOutHosp <- 2. / 11.77

# Rate hospital admission (for those ending up in ICU)
gToHospICU <- 1. / 3.

# Rate ICU admission
gToICU <- 1. / 2.6

# Probability of hospitalization given infection
pHosp_Inf <- 0.011

# Initial number of infected individuals
infected0 <- -1

# Over-dispersion (if < 0.01 observation process will be Poisson)
negbin_alpha <- 0

# Change-points for time spent in ICU
cps_gOutICU <- c()

# 2 x rate ICU -> R
gOutICUs <- c(-1)

# Change-points for probability of ICU given hospitalization
cps_pICU_Hosp <- c()

# 2 x rate ICU -> R (Positive value => fixed)
pICU_Hosps <- c(-1)

# Change-points for reproduction number
# 28 = May 20th, 54 = June 15th
cps_R0 <- c(28, 54)

# Three negative values, so R0 is estimated in all three periods
R0s <- c(-1, -1, -1)

# ------------------------------------------------------------------------------
# PREPARE THE ARGUMENTS TO PASS TO THE PROGRAM
# ------------------------------------------------------------------------------
build_changes <- function(cps, values) {
  n_cps <- length(cps)
  all_cps <- rep(0, 5)
  if (n_cps > 0) {
    all_cps[1:n_cps] <- cps
  }
  all_values <- rep(0, 6)
  all_values[1:(n_cps + 1)] <- values
  list(cps = all_cps, vals = all_values)
}

tmp <- build_changes(cps_gOutICU, gOutICUs)
all_cps_gOutICU <- tmp$cps
all_gOutICUs <- tmp$vals

tmp <- build_changes(cps_pICU_Hosp, pICU_Hosps)
all_cps_pICU_Hosp <- tmp$cps
all_pICU_Hosps <- tmp$vals

tmp <- build_changes(cps_R0, R0s)
all_cps_R0 <- tmp$cps
all_R0s <- tmp$vals

params <- c(fdata,
            sprintf("%d", population),
            sprintf("%f", dt),
            sprintf("%d", n_steps),
            sprintf("%d", remove_last_n),
            sprintf("%d", mcmc_steps),
            sprintf("%d", mcmc_adaptive_steps),
            sprintf("%d", mcmc_thinning),
            fmcmc,
            fsims,
            sprintf("%f", g1),
            sprintf("%f", g2),
            sprintf("%f", g3),
            sprintf("%f", gToHosp),
            sprintf("%f", gOutHosp),
            sprintf("%f", gToHospICU),
            sprintf("%f", gToICU),
            sprintf("%f", pHosp_Inf),
            sprintf("%f", infected0),
            sprintf("%f", negbin_alpha),
            paste(all_cps_gOutICU),
            paste(all_gOutICUs),
            paste(all_cps_pICU_Hosp),
            paste(all_pICU_Hosps),
            paste(all_cps_R0),
            paste(all_R0s))
if (length(params) != 53) {
  stop("Wrong number of parameters!")
}
arg_string <- paste(params, collapse = " ")

# Run program
system(paste("./covid.exe", arg_string))
