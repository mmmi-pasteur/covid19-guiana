// Distributed under the Boost Software License, Version 1.0.
//    (See accompanying file LICENSE)

#include <iostream>
#include <algorithm>
#include <boost/numeric/odeint/stepper/runge_kutta4.hpp>
#include <boost/numeric/odeint/integrate/integrate_times.hpp>
#include "mcmc.hpp"
using namespace std;
using namespace boost::numeric::odeint;
using namespace boost::numeric::odeint::detail;


// -----------------------------------------------------------------------------
// Observation process
// -----------------------------------------------------------------------------
double log_pois(size_t k, double lambda, double unused) {
  // Log of poisson pmf
  return k * log(lambda) - lambda - lgamma(k + 1);
}


double draw_pois(double lambda, double unused, mt19937_64& gen) {
  poisson_distribution<> pois(lambda);
  return pois(gen);
}


double log_neg_binom(size_t k, double m, double r) {
  // Log of negative binomial pmf
  double res = lgamma(r + k) - lgamma(k + 1) - lgamma(r);
  res += r * log(r / (r + m));
  res += k * log(m / (r + m));
  return res;
}


double draw_neg_binom(double m, double r, mt19937_64& gen) {
  // This exploits the fact that negative binomial distribution can be viewed
  // as a Gamma-Poisson mixture, i.e. a Poisson distribution with a mean
  // distributed as a gamma distribution
  // m = mean
  // r = overdispersion parameter
  double proba = r / (r + m);
  gamma_distribution<> gamma(r, (1.0 - proba) / proba);
  poisson_distribution<> pois(gamma(gen));
  return pois(gen);
}


// -----------------------------------------------------------------------------
// Data loading
// -----------------------------------------------------------------------------
vector<vector<size_t>> load_cols(string fname, size_t n_cols,
    size_t remove_last_n) {
  ifstream ifs(fname);
  if (!ifs.is_open()) {
    cout << "ERROR: file " << fname << " does not exist!" << endl;
    exit(1);
  }

  // Read file once to get number of rows
  string line;
  size_t n_lines = 0;
  while (getline(ifs, line)) {
    if (line == "") continue;
    ++n_lines;
  }

  // Reset to beginning of file  
  ifs.clear();
  ifs.seekg(0, ios::beg);

  // Read again to load data
  size_t n;
  vector<vector<size_t>> ns(n_lines - remove_last_n, vector<size_t>(n_cols, 0));
  size_t n_line = 0;
  while (getline(ifs, line)) {
    if (line == "") continue;
    if (n_line >= (n_lines - remove_last_n)) break;
    istringstream parser(line);
    for (size_t adv = 0; adv < n_cols; ++adv) {
      parser >> n;
      ns[n_line][adv] = n;
    }
    ++n_line;
  }
  ifs.close();
  return ns;
}


void load_mcmc_chain(std::string fname, size_t burn_in,
    vector<string>& chain_no_burn_in) {
  ifstream ifs(fname);
  if (!ifs.is_open()) {
    cout << "ERROR: file " << fname << " does not exist!" << endl;
    exit(1);
  }

  size_t line_count = 0;
  string line;
  while (!ifs.eof()) {
    getline(ifs, line);
    // Greater than since first line is header
    if (line_count > burn_in && line != "") {
      chain_no_burn_in.push_back(line);
    }
    ++line_count;
  }
  ifs.close();
}


// -----------------------------------------------------------------------------
// Constants
// -----------------------------------------------------------------------------
// Parameter names
const vector<string> param_names = {
  "g1", "g2", "g3", "gToHosp", "gOutHosp", "gToHospICU", "gToICU", "pHosp_Inf",
  "infected0", "negbin_alpha",
  "gOutICU_1", "gOutICU_2", "gOutICU_3", "gOutICU_4", "gOutICU_5", "gOutICU_6",
  "pICU_Hosp_1", "pICU_Hosp_2", "pICU_Hosp_3", "pICU_Hosp_4", "pICU_Hosp_5", "pICU_Hosp_6", 
  "R0_1", "R0_2", "R0_3", "R0_4", "R0_5", "R0_6"
};


// For each param: default value, min and max (used for MCMC priors)
const vector<vector<double>> param_defaults = {
  {1.0 / 4.0, 0.0, 10.0}, // 0: g1
  {1.0 / 1.0, 0.0, 10.0}, // 1: g2
  {1.0 / 3.0, 0.0, 10.0}, // 2: g3
  {1.0 / 4.0, 0.0, 10.0}, // 3: gToHosp
  {2.0 / 12.1, 0.0, 10.0}, // 4: gOutHosp
  {1.0 / 3.0, 0.0, 10.0}, // 5: gToHospICU
  {1.0 / 1.5, 0.0, 10.0}, // 6: gToICU
  {0.011, 0.0, 1.0}, // 7: pHosp_Inf
  {10.0, 0.0, 5e3}, // 8: infected0
  {1.0, 0.0, 2.0}, // 9: negbin_alpha (overdispersion parameter, NOT USED)
  {2.0 / 11.9, 0.0, 10.0}, // 10: gOutICU_1
  {2.0 / 11.9, 0.0, 10.0}, // 11: gOutICU_2
  {2.0 / 11.9, 0.0, 10.0}, // 12: gOutICU_3
  {2.0 / 11.9, 0.0, 10.0}, // 13: gOutICU_4
  {2.0 / 11.9, 0.0, 10.0}, // 14: gOutICU_5
  {2.0 / 11.9, 0.0, 10.0}, // 15: gOutICU_6
  {0.142, 0.0, 1.0}, // 16: pICU_Hosp_1
  {0.142, 0.0, 1.0}, // 17: pICU_Hosp_2
  {0.142, 0.0, 1.0}, // 18: pICU_Hosp_3
  {0.142, 0.0, 1.0}, // 19: pICU_Hosp_4
  {0.142, 0.0, 1.0}, // 20: pICU_Hosp_5
  {0.142, 0.0, 1.0}, // 21: pICU_Hosp_6
  {1.5, 0.0, 10.0}, // 22: R0_1
  {1.5, 0.0, 10.0}, // 23: R0_2
  {1.5, 0.0, 10.0}, // 24: R0_3
  {1.5, 0.0, 10.0}, // 25: R0_4
  {1.5, 0.0, 10.0}, // 26: R0_5
  {1.5, 0.0, 10.0} // 27: R0_6
};


// -----------------------------------------------------------------------------
// Configuration class (used to store data and settings used for inference)
// -----------------------------------------------------------------------------
struct Configuration {
  size_t n_data; // Number of data points
  size_t n_steps; // Number of time steps for simulations
  size_t mcmc_steps; // Total number of MCMC steps
  size_t mcmc_adaptive_steps; // Number of adaptive MCMC steps
  size_t mcmc_thinning; // Thinning factor
  double dt; // ODE integration step
  double population; // Population size
  string simulator_type; // "pois" (Poisson) or "neg_bin" (NegativeBinomial)
  string fmcmc; // Output file for MCMC
  string fsims; // Output file for simulations
  vector<size_t> spoints_gOutICU; // Switch-points for time spent in ICU
  vector<size_t> spoints_pICU_Hosp; // Switch-points for proba ICU
  vector<size_t> spoints_R0; // Switch-points for reproduction number

  // Containers to store data
  vector<size_t> newHosp;
  vector<size_t> newICU;
  vector<size_t> inHosp;
  vector<size_t> inICU;

  // Parameters
  size_t n_params;
  vector<size_t> mapping;
  vector<double> params;

  Configuration(char* argv[]) {
    // Fixed parameters
    string fdata = argv[1];
    population = atof(argv[2]);
    assert(population > 0.0);
    dt = atof(argv[3]);
    assert(dt > 0.0 && dt < 1.0);
    n_steps = atoi(argv[4]);
    assert(n_steps < 1000000);
    size_t remove_last_n = atoi(argv[5]);
    assert(remove_last_n < 1000000);
    mcmc_steps = atoi(argv[6]);
    assert(mcmc_steps < 1000000000);
    mcmc_adaptive_steps = atoi(argv[7]);
    assert(mcmc_adaptive_steps < 1000000000);
    mcmc_thinning = atoi(argv[8]);
    assert(mcmc_thinning >= 1 && mcmc_thinning < 1000000000);
    fmcmc = argv[9];
    fsims = argv[10];

    load_data(fdata, remove_last_n);

    // Parameters for inference
    params.resize(28, 0.0);
    for (size_t curr = 0; curr < 9; ++curr) { // Up to overdispersion param
      add_param(atof(argv[11 + curr]), curr);
    }

    simulator_type = "neg_bin";
    params[9] = atof(argv[20]); // Overdispersion param
    if (params[9] < 0.0) {
      params[9] = param_defaults[9][0];
      mapping.push_back(9);
    } else if (params[9] < 0.01) {
      simulator_type = "poisson";
    }

    // Change-points gOutHosp
    for (size_t curr = 21; curr < 26; ++curr) {
      size_t change_point = atoi(argv[curr]);
      if (change_point == 0) break;
      spoints_gOutICU.push_back(change_point);
    }
    for (size_t curr = 0; curr <= spoints_gOutICU.size(); ++curr) {
      add_param(atof(argv[26 + curr]), 10 + curr);
    }

    // Change-points pICU_Hosp
    for (size_t curr = 32; curr < 37; ++curr) {
      size_t change_point = atoi(argv[curr]);
      if (change_point == 0) break;
      spoints_pICU_Hosp.push_back(change_point);
    }
    for (size_t curr = 0; curr <= spoints_pICU_Hosp.size(); ++curr) {
      add_param(atof(argv[37 + curr]), 16 + curr);
    }

    // Change-points R0
    for (size_t curr = 43; curr < 48; ++curr) {
      size_t change_point = atoi(argv[curr]);
      if (change_point == 0) break;
      spoints_R0.push_back(change_point);
    }
    for (size_t curr = 0; curr <= spoints_R0.size(); ++curr) {
      add_param(atof(argv[48 + curr]), 22 + curr);
    }

    n_params = mapping.size();
  }

  // Param getters
  double get_g1() { return params[0]; }
  double get_g2() { return params[1]; }
  double get_g3() { return params[2]; }
  double get_gToHosp() { return params[3]; }
  double get_gOutHosp() { return params[4]; }
  double get_gToHospICU() { return params[5]; }
  double get_gToICU() { return params[6]; }
  double get_pHosp_Inf() { return params[7]; }
  double get_infected_0() { return params[8]; }

  double get_overdispersion(double mean) {
    if (simulator_type == "neg_bin") return pow(mean, params[9]);
    return 0.0;
  }

  double get_gOutICU(size_t it) {
    // No change points
    if (spoints_gOutICU.size() == 0) return params[10];
    // Before first change point
    if (it < spoints_gOutICU[0]) return params[10];
    // Intermediate change points
    for (size_t curr = 1; curr < spoints_gOutICU.size(); ++curr) {
      if ( (it >= spoints_gOutICU[curr - 1]) && (it < spoints_gOutICU[curr]) ) {
        return params[10 + curr];
      }
    }
    // After last change point
    return params[10 + spoints_gOutICU.size()];
  }

  double get_pICU_Hosp(size_t it) {
    // No change points
    if (spoints_pICU_Hosp.size() == 0) return params[16];
    // Before first change point
    if (it < spoints_pICU_Hosp[0]) return params[16];
    // Intermediate change points
    for (size_t curr = 1; curr < spoints_pICU_Hosp.size(); ++curr) {
      if ( (it >= spoints_pICU_Hosp[curr - 1]) && (it < spoints_pICU_Hosp[curr]) ) {
        return params[16 + curr];
      }
    }
    // After last change point
    return params[16 + spoints_pICU_Hosp.size()];
  }

  double get_R0(size_t it) {
    // No change points
    if (spoints_R0.size() == 0) return params[22];
    // Before first change point
    if (it < spoints_R0[0]) return params[22];
    // Intermediate change points
    for (size_t curr = 1; curr < spoints_R0.size(); ++curr) {
      if ( (it >= spoints_R0[curr - 1]) && (it < spoints_R0[curr]) ) {
        return params[22 + curr];
      }
    }
    // After last change point
    return params[22 + spoints_R0.size()];
  }

  bool must_update_internal_state(size_t param_i_updated) {
    // Do not need to solve ODE system when updating overdispersion parameter
    return param_i_updated != 9;
  }

  void add_param(double value, size_t real_param_i) {
    // Helper function to populate vector of parameters to estimate via MCMC
    if (value > 0.0) {
      params[real_param_i] = value;
    } else {
      params[real_param_i] = param_defaults[real_param_i][0];
      mapping.push_back(real_param_i);
    }
  }

  void load_data(string fname, size_t remove_last_n) {
    // Helper function to load data
    vector<vector<size_t>> all_cols = load_cols(fname, 4, remove_last_n);
    n_data = all_cols.size();
    newHosp.resize(n_data, 0);
    newICU.resize(n_data, 0);
    inHosp.resize(n_data, 0);
    inICU.resize(n_data, 0);
    for (size_t i = 0; i < n_data; ++i) {
      newHosp[i] = all_cols[i][0];
      newICU[i] = all_cols[i][1];
      inHosp[i] = all_cols[i][2];
      inICU[i] = all_cols[i][3];
    }
  }
};


//------------------------------------------------------------------------------
// Deterministic SEIR (classes used by Boost ODE solver)
//------------------------------------------------------------------------------
typedef vector<double> StateD;


struct Observer {
  vector<StateD> &trajectory;
  size_t pos;

  Observer(vector<StateD>& trajectory): trajectory(trajectory), pos(0) { }
  void operator()(const StateD& x, double t) {
    trajectory[pos++] = x;
    pos = pos % trajectory.size();
  }
};


struct DSEIR {
  Configuration *config;

  DSEIR(Configuration& config): config(&config) { }

  void operator()(const StateD& x, StateD& dxdt, const double t) {
    double D = (1. / config->get_g2() + 1. / config->get_g3()); // Infectious period
    double beta = config->get_R0(size_t(t)) / D; // Transmission rate
    double gOutICU = config->get_gOutICU(size_t(t));
    double pICU_Hosp = config->get_pICU_Hosp(size_t(t));

    double S_to_E1 = beta * x[0] * (x[2] + x[3] + x[4]) / config->population;
    double E1_to_E2 = config->get_g1() * x[1];
    double E2_to_I = config->get_g2() * x[2];
    // Mild cases
    double E2_to_IMild = (1.0 - config->get_pHosp_Inf()) * E2_to_I;
    double IMild_to_R = config->get_g3() * x[3];
    // Severe cases
    double E2_to_IHosp = config->get_pHosp_Inf() * E2_to_I;
    double IHosp_to_IBar = config->get_g3() * x[4];
    double IHosp_to_IBarHosp = (1. - pICU_Hosp) * IHosp_to_IBar;
    double IBarHosp_to_H1 = config->get_gToHosp() * x[5];
    double H1_to_H2 = config->get_gOutHosp() * x[6];
    double H2_to_R = config->get_gOutHosp() * x[7];
    double IHosp_to_IBarICU = pICU_Hosp * IHosp_to_IBar;
    double IBarICU_to_HICU = config->get_gToHospICU() * x[8];
    double HICU_to_ICU1 = config->get_gToICU() * x[9];
    double ICU1_to_ICU2 = gOutICU * x[10];
    double ICU2_to_R = gOutICU * x[11];
    dxdt[0] = -S_to_E1; // S
    dxdt[1] = S_to_E1 - E1_to_E2; // E1
    dxdt[2] = E1_to_E2 - E2_to_I; // E2
    dxdt[3] = E2_to_IMild - IMild_to_R; // IMild
    dxdt[4] = E2_to_IHosp - IHosp_to_IBar; // IHosp
    dxdt[5] = IHosp_to_IBarHosp - IBarHosp_to_H1; // IBarHosp
    dxdt[6] = IBarHosp_to_H1 - H1_to_H2; // H1
    dxdt[7] = H1_to_H2 - H2_to_R; // H2
    dxdt[8] = IHosp_to_IBarICU - IBarICU_to_HICU; // IBarICU
    dxdt[9] = IBarICU_to_HICU - HICU_to_ICU1; // HICU
    dxdt[10] = HICU_to_ICU1 - ICU1_to_ICU2; // ICU1
    dxdt[11] = ICU1_to_ICU2 - ICU2_to_R; // ICU2
    dxdt[12] = IMild_to_R + H2_to_R + ICU2_to_R; // R
    dxdt[13] = E2_to_I; // Cumulative number of infectious (NOT USED)
    dxdt[14] = IBarHosp_to_H1 + IBarICU_to_HICU; // Cumulative number of hospital admissions
    dxdt[15] = HICU_to_ICU1; // Cumulative number of ICU admissions
  }
};


//------------------------------------------------------------------------------
// Model
//------------------------------------------------------------------------------
struct Model: public MCMCAbstractModel {
  Configuration *config; // Configuration (containing data & settings)
  DSEIR seir; // ODE system
  Observer obs; // Object used by Boost to store trajectories
  size_t tot_steps; // Number of time steps to simulate
  size_t ode_steps; // Number of time steps for ODE
  double propE1; // Proportion of infected in E1 compartment at t = 0
  double propE2; // Proportion of infected in E2 compartment at t = 0
  double propI; // Proportion of infected in I compartment at t = 0
  vector<double> ts; // Time points
  vector<StateD> sim; // Container for trajectories
  vector<double> exp_newHosp; // Expected number of new hospitalizations
  vector<double> exp_newICU; // Expected number of new ICU admissions
  vector<double> exp_inHosp; // Expected number of hospitalized individuals
  vector<double> exp_inICU; // Expected number of individuals in ICU
  vector<double> obs_newHosp; // Simulated new hospitalizations
  vector<double> obs_newICU; // Simulated new ICU admissions
  vector<double> obs_inHosp; // Simulated number of hospitalized individuals
  vector<double> obs_inICU; // Simulated number of individuals in ICU
  double (*log_proba)(size_t, double, double); // Pointer to logproba function
  double (*draw)(double, double, mt19937_64&); // Pointer to draw function
  runge_kutta4<StateD> stepper; // Runge-Kutta 4 solver

  Model(Configuration& config): config(&config), seir(config), obs(sim) {
    // Initialize observation process
    if (config.simulator_type == "poisson") {
      log_proba = &log_pois;
      draw = &draw_pois;
    } else {
      log_proba = &log_neg_binom;
      draw = &draw_neg_binom;
    }

    // Preallocate containers
    tot_steps = config.n_data;
    ode_steps = tot_steps + 1;
    ts.resize(ode_steps, 0.0);
    for (size_t curr = 0; curr < ode_steps; ++curr) ts[curr] = curr;
    sim.resize(ode_steps, StateD(16, 0.0));
    exp_newHosp.resize(tot_steps, 0.0);
    exp_newICU.resize(tot_steps, 0.0);
    exp_inHosp.resize(tot_steps, 0.0);
    exp_inICU.resize(tot_steps, 0.0);
    obs_newHosp.resize(tot_steps, 0.);
    obs_newICU.resize(tot_steps, 0.);
    obs_inHosp.resize(tot_steps, 0.);
    obs_inICU.resize(tot_steps, 0.);

    // For initial conditions
    double den = config.get_g2() * config.get_g3()
      + config.get_g1() * config.get_g3()
      + config.get_g1() * config.get_g2();
    propE1 = config.get_g2() * config.get_g3() / den;
    propE2 = config.get_g1() * config.get_g3() / den;
    propI = config.get_g1() * config.get_g2() / den;

    update_internal_state();
  }

  size_t get_n_params() {
    return config->n_params;
  }

  double get_param(size_t i) {
    // Get parameter at index i
    size_t real_i = config->mapping[i];
    return config->params[real_i];
  }

  void set_param(size_t i, double value) {
    // Set parameter at index i (solving ODE system if needed)
    size_t real_i = config->mapping[i];
    config->params[real_i] = value;
    if (config->must_update_internal_state(real_i)) {
      update_internal_state();
    }
  }

  void update_internal_state() {
    // Set initial conditions
    StateD initial_state(16, 0.0);
    double inf0 = config->get_infected_0();
    initial_state[0] = config->population - inf0; // S
    initial_state[1] = inf0 * propE1; // E1
    initial_state[2] = inf0 * propE2; // E2
    initial_state[3] = inf0 * propI * (1. - config->get_pHosp_Inf());
    initial_state[4] = inf0 * propI * config->get_pHosp_Inf();

    // Solve ODE system
    integrate_times(
      stepper, seir, initial_state, ts.begin(), ts.end(), config->dt, obs
    );

    // Compute expected numbers
    for (size_t i = 0; i < tot_steps; ++i) {
      exp_newHosp[i] = sim[i + 1][14] - sim[i][14];
      exp_newICU[i] = sim[i + 1][15] - sim[i][15];
      exp_inHosp[i] = sim[i + 1][6] + sim[i + 1][7] + sim[i + 1][9];
      exp_inICU[i] = sim[i + 1][10] + sim[i + 1][11];
    }
  }

  double compute_log_likelihood() {
    double r1, r2, r3, r4;
    double ll = 0.0;
    for (size_t i = 0; i < config->n_data; ++i) {
      r1 = config->get_overdispersion(exp_newHosp[i]);
      r2 = config->get_overdispersion(exp_newICU[i]);
      r3 = config->get_overdispersion(exp_inHosp[i]);
      r4 = config->get_overdispersion(exp_inICU[i]);
      ll += log_proba(config->newHosp[i], exp_newHosp[i], r1) +
        log_proba(config->newICU[i], exp_newICU[i], r2) +
        log_proba(config->inHosp[i], exp_inHosp[i], r3) +
        log_proba(config->inICU[i], exp_inICU[i], r4);
    }
    return ll;
  }

  void observe_state(mt19937_64& gen) {
    // Draw observed numbers
    double r;
    for (size_t i = 0; i < tot_steps; ++i) {
      r = config->get_overdispersion(exp_newHosp[i]);
      obs_newHosp[i] = draw(exp_newHosp[i], r, gen);
      r = config->get_overdispersion(exp_newICU[i]);
      obs_newICU[i] = draw(exp_newICU[i], r, gen);
      r = config->get_overdispersion(exp_inHosp[i]);
      obs_inHosp[i] = draw(exp_inHosp[i], r, gen);
      r = config->get_overdispersion(exp_inICU[i]);
      obs_inICU[i] = draw(exp_inICU[i], r, gen);
    }
  }

  void simulate(string mcmc_file, string out_file, size_t burn_in,
      size_t n_param_sets, size_t n_reps, size_t seed) {
    // Simulate epidemics with parameters drawn from posterior distribution

    // Load MCMC chain
    vector<string> chain_no_burn_in;
    load_mcmc_chain(mcmc_file, burn_in, chain_no_burn_in);

    // Check that after removing burn_in there are still enough iterations
    assert(chain_no_burn_in.size() > n_param_sets);

    // Reshuffle lines
    mt19937_64 gen(seed);
    shuffle(chain_no_burn_in.begin(), chain_no_burn_in.end(), gen);

    ofstream o_sims(out_file);
    o_sims << "sim,time_i,iHosp,iICU,inHosp,inICU,prop_immune" << endl;

    // Preallocate containers
    tot_steps = config->n_steps;
    ode_steps = tot_steps + 1;
    ts.resize(ode_steps, 0.0);
    for (size_t curr = 0; curr < ode_steps; ++curr) ts[curr] = curr;
    sim.resize(ode_steps, StateD(16, 0.0));
    exp_newHosp.resize(tot_steps, 0.0);
    exp_newICU.resize(tot_steps, 0.0);
    exp_inHosp.resize(tot_steps, 0.0);
    exp_inICU.resize(tot_steps, 0.0);
    obs_newHosp.resize(tot_steps, 0.);
    obs_newICU.resize(tot_steps, 0.);
    obs_inHosp.resize(tot_steps, 0.);
    obs_inICU.resize(tot_steps, 0.);

    size_t n_params = get_n_params();
    for (size_t curr_p = 0; curr_p < n_param_sets; ++curr_p) {
      cout << "Parameter set: " << (curr_p + 1) << endl;
      // Set parameters
      double d_to_skip;
      char c_to_skip;
      istringstream parser(chain_no_burn_in[curr_p]);
      parser >> d_to_skip >> c_to_skip; // iteration and comma
      parser >> d_to_skip >> c_to_skip; // loglik and comma
      for (size_t curr_i = 0; curr_i < n_params; ++curr_i) {
        size_t real_i = config->mapping[curr_i];
        parser >> config->params[real_i] >> c_to_skip; // param and comma
      }

      // Simulate epidemic
      update_internal_state();

      for (size_t rep = 0; rep < n_reps; ++rep) {
        observe_state(gen);
        for (size_t i = 0; i < tot_steps; ++i) {
          o_sims << (n_reps * curr_p + rep) << "," << i << "," <<
            obs_newHosp[i] << "," << obs_newICU[i] << "," <<
            obs_inHosp[i] << "," << obs_inICU[i] << "," <<
            (1. - (sim[i + 1][0] / config->population)) << endl;
        }
      }
    }
    o_sims.close();
  }
};


//------------------------------------------------------------------------------
// Helper function to run inference
//------------------------------------------------------------------------------
void run(char* argv[]) {
  // Build configuration
  Configuration config(argv);

  // Print info
  cout << "----------------------------------------" << endl;
  cout << "CONFIGURATION:" << endl;
  cout << "----------------------------------------" << endl;
  cout << "Data from: " << argv[1] << endl;
  cout << "MCMC output: " << config.fmcmc << endl;
  cout << "Simulations: " << config.fsims << endl;
  cout << endl;
  cout << "Data points: " << config.n_data << endl;
  cout << "remove_last_n: " << argv[5] << endl;
  cout << "population: " << config.population << endl;
  cout << "dt: " << config.dt << endl;
  cout << "n_steps: " << config.n_steps << endl;
  cout << "mcmc_steps: " << config.mcmc_steps << endl;
  cout << "mcmc_adaptive_steps: " << config.mcmc_adaptive_steps << endl;
  cout << "mcmc_thinning: " << config.mcmc_thinning << endl;
  cout << "simulator_type: " << config.simulator_type << endl;
  cout << "----------------------------------------" << endl;
  cout << "Parameters:" << endl;
  cout << "----------------------------------------" << endl;
  for (size_t curr = 0; curr < config.params.size(); ++curr) {
    cout << param_names[curr] << ": " << config.params[curr];
    string to_add = "";
    for (size_t param_i = 0; param_i < config.n_params; ++param_i) {
      if (config.mapping[param_i] == curr) {
        to_add += " [*** Parameter to estimate ***]";
      }
    }
    cout << to_add << endl;
  }
  cout << endl;
  cout << "Change-points gOutICU: ";
  if (config.spoints_gOutICU.size() == 0) {
    cout << "None" << endl;
  } else {
    for (size_t cpoint : config.spoints_gOutICU) cout << cpoint << " ";
    cout << endl;
  }
  cout << "Change-points pICU_Hosp: ";
  if (config.spoints_pICU_Hosp.size() == 0) {
    cout << "None" << endl;
  } else {
    for (size_t cpoint : config.spoints_pICU_Hosp) cout << cpoint << " ";
    cout << endl;
  }
  cout << "Change-points R0: ";
  if (config.spoints_R0.size() == 0) {
    cout << "None" << endl;
  } else {
    for (size_t cpoint : config.spoints_R0) cout << cpoint << " ";
    cout << endl;
  }
  cout << endl;
  cout << "N. params to estimate: " << config.n_params << endl;
  cout << "----------------------------------------" << endl;

  // Build model
  Model model(config);

  // Build MCMC object and assign names and priors
  MCMC mcmc(model);
  cout << "Priors:" << endl;
  cout << "----------------------------------------" << endl;
  for (size_t curr = 0; curr < config.n_params; ++curr) {
    size_t real_i = config.mapping[curr];
    double lower = param_defaults[real_i][1];
    double upper = param_defaults[real_i][2];
    cout << param_names[real_i] << ": U(" << lower << ", " << upper << ")"
      << endl;
    mcmc.set_parameter_name(curr, param_names[real_i]);
    mcmc.set_prior(curr, "uniform", lower, upper);
  }
  cout << "----------------------------------------" << endl;

  // Inference (it uses the first 'mcmc_adaptive_steps' to optimize the
  // step size for each parameter)
  mcmc.run_adaptive(
    config.mcmc_steps, config.mcmc_adaptive_steps, config.mcmc_thinning,
    config.fmcmc
  );

  // Simulations (burn_in = half MCMC steps)
  size_t burn_in = config.mcmc_steps / config.mcmc_thinning / 2;
  // 100 = number of parameter sets drawn from posterior distribution
  // 5 = number of repeats for each parameter set
  // 777 = RNG seed
  model.simulate(config.fmcmc, config.fsims, burn_in, 100, 5, 777);
}


//------------------------------------------------------------------------------
// Main
//------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
  if (argc != 54) {
    cout << "ERROR: Expecting 53 input parameters!" << endl;
    exit(1);
  }
  run(argv);
  exit(0);
}
